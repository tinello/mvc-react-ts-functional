import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
import Provider from '../core/Provider';
import AppPresenter from './AppPresenter';

const provider = Provider;
const appPresenter = new AppPresenter(provider);

test('renders learn react link', () => {
  render(<App presenter={appPresenter} />);
  const linkElement = screen.getByText(/Modelo Vista Presentador - React TS Functional/i);
  expect(linkElement).toBeInTheDocument();
});
