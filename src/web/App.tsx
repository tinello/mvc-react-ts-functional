import React, { useState } from 'react';
import './App.css';
import AppPresenter, { IAppView } from './AppPresenter';

interface AppProps {
  presenter: AppPresenter;
};
interface IApp extends React.FC<AppProps> {
};

const App: IApp = (props: AppProps) => {

  const [datos, setDatos] = useState({
    loading: false,
    error: {
      message: '',
    },
    form: {
      codigo: '',
      firstName: '',
      lastName: '',
      email: '',
      telephone: '',
    }
  });

  const presenter = props.presenter;

  const appView: IAppView = {
    showLoading: () => {setDatos({...datos, loading: true, error: {message: '',}});},
    hideLoading: () => {setDatos({...datos, loading: false, error: {message: '',}});},
    showError: (error: Error) => {setDatos({...datos, loading: false, error: error});},
    setClientId: (id: string) => {
      setDatos({
        ...datos,
        form: {
            ...datos.form,
            codigo: id,
        }
      });
    }
  };
  presenter.setView(appView);

  const handleClick = () => {
    presenter.createCliente(datos.form);
  };

  const handlerChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    setDatos({
        ...datos,
        form: {
            ...datos.form,
            [e.target.name]: e.target.value,
        }
    });
  }

  return (
    <div>
        <h1>Modelo Vista Presentador - React TS Functional</h1>
        <h2>Registrar persona</h2>
        <div>
            <h3>Codigo:</h3>
            <input type="text" id="codigo" name="codigo" onChange={handlerChange} value={datos.form.codigo} />
        </div>
        <div>
            <h3>Nombre:</h3>
            <input type="text" id="firstName" name="firstName" onChange={handlerChange} value={datos.form.firstName} />
        </div>
        <div>
            <h3>Apellido:</h3>
            <input type="text" id="lastName" name="lastName" onChange={handlerChange} value={datos.form.lastName} />
        </div>
        <div>
            <h3>Email:</h3>
            <input type="text" id="email" name="email" onChange={handlerChange} value={datos.form.email} />
        </div>
        <div>
            <h3>Telefono:</h3>
            <input type="text" id="telephone" name="telephone" onChange={handlerChange} value={datos.form.telephone} />
        </div>
        <div>
            <input type="button" value="Registrar" id="registerButton" onClick={handleClick} />
            {datos.loading && (
              <span className="text-danger">Creando cliente!!!</span>
            )}
        </div>
        {datos.error && (
          <p className="text-danger">{datos.error.message}</p>
        )}
      </div>
  );
};

export default App;
