import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './web/App';
import reportWebVitals from './reportWebVitals';
import Provider from './core/Provider';
import AppPresenter from './web/AppPresenter';

const provider = Provider;
const appPresenter = new AppPresenter(provider);

ReactDOM.render(
  <React.StrictMode>
    <App presenter={appPresenter} />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
